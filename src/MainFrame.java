import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	public JTextField textField;
	private JButton btnCogerDelClippboard;
	private JButton button;
	
	public MainFrame() {
		setTitle("Seleccione acci�n");
		setIconImage(new ImageIcon("codator.gif").getImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new FlowLayout(FlowLayout.LEFT, 7, 15));
		
		Box verticalBox = Box.createVerticalBox();
		getContentPane().add(verticalBox);
		
		Box horizontalBox = Box.createHorizontalBox();
		horizontalBox.setBorder(new TitledBorder(null, "Seleccionar archivo", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		verticalBox.add(horizontalBox);
		
		JPanel panel_1 = new JPanel();
		horizontalBox.add(panel_1);
		
		JLabel lblAbrirDesde = new JLabel("Abrir desde: ");
		panel_1.add(lblAbrirDesde);
		
		textField = new JTextField();
		panel_1.add(textField);
		textField.setColumns(40);
		
		Component horizontalStrut = Box.createHorizontalStrut(10);
		panel_1.add(horizontalStrut);
		
		button = new JButton("...");
		panel_1.add(button);
		
		Component verticalStrut = Box.createVerticalStrut(15);
		verticalBox.add(verticalStrut);
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		horizontalBox_1.setBorder(new TitledBorder(null, "Usar clipboard", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		verticalBox.add(horizontalBox_1);
		
		JPanel panel = new JPanel();
		horizontalBox_1.add(panel);
		
		btnCogerDelClippboard = new JButton("Coger del Clipboard");
		panel.add(btnCogerDelClippboard);
		
		pack();
		setLocation(200, 200);
		setResizable(false);
		setVisible(true);
	}
	
	public void addController(ActionListener path, ActionListener clip) {
		button.addActionListener(path);
		btnCogerDelClippboard.addActionListener(clip);
	}

}
