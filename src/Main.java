import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.JFileChooser;
import javax.swing.UIManager;

public class Main {

	public static void main(String[] args) throws Throwable {
		setSystemLookAndFeel();

		final MainFrame frame = new MainFrame();
		frame.addController(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) { // seleccionar archivo
				try {
					String str = frame.textField.getText();
					if (str == null || str.equals(""))
						str = ".";

					String pathFile = chooseFile("Elige archivo", str, null);

					String cadena;
					String res = "String str = ";
					boolean first = true;

					FileReader f = new FileReader(pathFile);
					BufferedReader b = new BufferedReader(f);
					while ((cadena = b.readLine()) != null) {
						if (!first)
							res += "+ ";
						else
							first = false;

						cadena = cadena.replace("\\", "\\\\");
						cadena = cadena.replace("\"", "\\\"");
						cadena = cadena.replace("\t", "\\t");

						res += ("\"" + cadena + "\\n\"\n");
					}
					res = res.substring(0, res.length() - 1);
					res += ";";

					b.close();

					StringSelection s = new StringSelection(res);
					Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
					clpbrd.setContents(s, null);

					System.out.println(res);
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}

		}, new ActionListener() { // seleccionar del clipboard

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String res = "String str = ";
					boolean first = true;

					Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
					Transferable ct = clpbrd.getContents(null);
					String data = (String) ct.getTransferData(DataFlavor.stringFlavor);

					String cad[] = data.split("[\\n]");

					for (String cadena : cad) {
						if (!first)
							res += "+ ";
						else
							first = false;

						cadena = cadena.replace("\\", "\\\\");
						cadena = cadena.replace("\"", "\\\"");
						cadena = cadena.replace("\t", "\\t");

						res += ("\"" + cadena + "\\n\"\n");
					}
					res = res.substring(0, res.length() - 1);
					res += ";";
					
					StringSelection s = new StringSelection(res);
					Clipboard clpbrd2 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clpbrd2.setContents(s, null);

					System.out.println(res);
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}

		});

	}

	private static String chooseFile(String title, String pathInit, Component frameCaller) {
		JFileChooser chooser;
		File fInit = new File(pathInit);
		String res = fInit.getAbsolutePath();

		chooser = new JFileChooser();
		chooser.setCurrentDirectory(fInit);
		chooser.setDialogTitle(title);
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);

		if (chooser.showOpenDialog(frameCaller) == JFileChooser.APPROVE_OPTION)
			res = chooser.getSelectedFile().getAbsolutePath();
		else
			return null;

		return res;
	}

	private static boolean setSystemLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
}
